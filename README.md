# NAME

HPC::Runner::Web::run::Plugin::MCE - Run the jobs in your lab notebook using MCE

# SYNOPSIS

    use HPC::Runner::Web::run::Plugin::MCE;

# DESCRIPTION

HPC::Runner::Web::run::Plugin::MCE is a plugin to run notebooks with [MCE](https://metacpan.org/pod/MCE). Please see [MCE::Core](https://metacpan.org/pod/MCE::Core) and [HPC::Runner::MCE](https://metacpan.org/pod/HPC::Runner::MCE) for more usage details

# AUTHOR

Jillian Rowe &lt;jillian.e.rowe@gmail.com>

# COPYRIGHT

Copyright 2015- Jillian Rowe

# LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# SEE ALSO
