package HPC::Runner::Web::run::Plugin::MCE;

use 5.008_005;
our $VERSION = '0.04';
use Data::Dumper;

use Moose::Role;
use HPC::Runner::Web::run::MCE;
use Data::Dumper;

use namespace::autoclean;

sub run_notebook {
    my $self = shift;
    my $args = shift;
    my $tags = shift;

    $DB::single=2;
    my $mce = HPC::Runner::Web::run::MCE->new($args);
    if($tags){
        $mce->tags($tags);
    }
    else{
        $mce->tags([]);
    }
    $DB::single=2;
    $mce->env_log;
    $mce->go;
}

1;
__END__

=encoding utf-8

=head1 NAME

HPC::Runner::Web::run::Plugin::MCE - Run the jobs in your lab notebook using MCE

=head1 SYNOPSIS

  use HPC::Runner::Web::run::Plugin::MCE;

=head1 DESCRIPTION

HPC::Runner::Web::run::Plugin::MCE is a plugin to run notebooks with L<MCE>. Please see L<MCE::Core> and L<HPC::Runner::MCE> for more usage details

=head1 AUTHOR

Jillian Rowe E<lt>jillian.e.rowe@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2015- Jillian Rowe

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
