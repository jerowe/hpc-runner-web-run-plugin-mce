package HPC::Runner::Web::run::MCE;

use 5.008_005;
our $VERSION = '0.04';
use Data::Dumper;

use Moose;

extends qw(HPC::Runner::Web::Log HPC::Runner::MCE);
with 'MooseX::Getopt';
use namespace::autoclean;

has '+version' => (
    is => 'rw',
    isa => 'Str',
);

before 'process_lines' => sub {
    my $self = shift;
    my $line = shift;


    return unless $line =~ m/^#NOTE/;
    $self->add_cmd($line);
    return;
};

sub go{
    my $self = shift;

    #my $dt1 = DateTime->now();
    my $dt1 = $self->dt;

    $self->logfile($self->set_logfile);
    $self->append_logfile("-MAIN.md");

    #$self->prepend_logfile("MAIN_");
    $self->log($self->init_log);
    $self->mce->spawn;

    #MCE specific
    #$self->job_tags(['sample']);
    $self->parse_file_mce;

    $DB::single=2;
# MCE workers dequeue 2 elements at a time. Thus the reason for * 2.
    $self->queue->enqueue((undef) x ($self->procs * 2));
# MCE will automatically shutdown after running for 1 or no args.
    $self->mce->run(1);
    #End MCE specific

    my $dt2 = DateTime->now();
    my $duration = $dt2 - $dt1;
    my $format = DateTime::Format::Duration->new(
        pattern => '%Y years, %m months, %e days, %H hours, %M minutes, %S seconds'
    );

    $DB::single=2;
    #$self->log->info("Total execution time ".$format->format_duration($duration));
    $self->log_main_messages('info', "Total execution time ".$format->format_duration($duration));
    return;
}

1;
__END__

=encoding utf-8

=head1 NAME

HPC::Runner::Web::run::MCE - Blah blah blah

=head1 SYNOPSIS

  use HPC::Runner::Web::run::MCE;

=head1 DESCRIPTION

HPC::Runner::Web::run::MCE is

=head1 AUTHOR

Jillian Rowe E<lt>jillian.e.rowe@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2015- Jillian Rowe

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
